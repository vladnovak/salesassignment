Shopping categories

CODE TASK FOR 100 PTS

You are working for a retailing company that sells lots of products. 
The management asked you to prepare a report that will bring great insight for the business. 
What they require is to compute the total income for the category as well as the number of the products sold per category. 
The categories are structured into a tree. Every category may have zero or more subcategories and each subcategory may have zero or more subcategories and so on. Every product may belong to zero or more categories. Each product holds information about its sales. Every sale stores information about the number of sold products and it individual price.

Your assignment is to fix compilation issues and then implement two methods:

•	int getNoOfProductsSold(String category) - returns the number of products sold in the given category

•	int getTotalIncome(String category) - returns the value of products sold in the given category

If the requested category does not exist both methods should return 0.

All the information about the categories, products, sales can be fetched using REST API. Every service has its cost:

•	/product - get all products, cost: 50

•	/product/{productId} - get product by id: cost: 3

•	/sales - get all sales, cost: 40

•	/product/{productId}/sales - get sales for product, cost: 3

•	/product/{productId}/categories - get categories for product, cost: 1

•	/category - get all categories, cost: 100

•	/category/{categoryName} - get category by name, cost: 1

More detailed description of the REST API you can find in the documentation tab.

To make a call via REST use RestTemplate object and its getForEntity(String, Class) method.

Example: XYZ xyz = new RestTemplate().getForEntity("/example", XYZ.class)

For the sake of this assignment assume that the number of all categories is less than 10 and the number of all products is less than 15.
Implement your solution in the most efficient way (with the lowest cost of api calls).

Example:

Input:

•	three categories: A, B, C

•	B is a subcategory for A



•	three product: X, Y, Z

•	X belongs to A

•	Y belongs to B

•	Z belongs to C



•	X was sold twice: 10 items per 10$, 5 items per 9$

•	Y was sold once: 3 items per 5$

•	Z was sold once: 6 items per 4$


Excepted output:

•	getNoOfProductsSold("C") - should returns: 6 (only Z product was sold in category C)

•	getTotalIncome("A") - should returns: 160 (X and Y are held within A, 10*10$ + 5*9$ + 3*5$ = 160$)



Example unit tests

Solution solution = new Solution(new RestTemplate(new TestSet.Default())); //verifies compilation

Documentation

/product
Get all products, cost: 50
Returns list of all Product objects stored in the datastore


/product/{productId}
Get product by id: cost: 3
Returns single Product object matched by passed productId or null if Product does not exist


/sales
Get all sales, cost: 40
Returns list of all Sale objects stored in the datastore


/product/{productId}/sales
Get sales for product, cost: 3
Return list of Sale objects assigned to the Product object with matched productId


/product/{productId}/categories
Get categories for product, cost: 1
Return list of String objects assigned to the Product with matched productId as categories


/category
Get all categories, cost: 100
Returns list of all Category objects stored in the datastore


/category/{categoryName}
Get category by name, cost: 1
Returns single Category object matched by passed categoryName or null if Category does not exist


import java.util.*;

import java.util.function.Function;

import java.util.stream.*;

class Solution {

    private final RestTemplate restTemplate;

    public Solution(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public int getTotalIncome(String category) {
        return 0;
    }

    public int getNoOfProductsSold(String category) {
        return 0;
    }
}

class Category {

    private final String name;
    private List<Integer> productsIds = List.of();
    private List<String> subCategoriesIds = List.of();

    public Category(String name) {
        this.name = name;
    }

    public void setProductsIds(List<Integer> productsIds) {
        this.productsIds = productsIds;
    }

    public List<String> getSubCategoriesIds() {
        return subCategoriesIds;
    }

    public void setSubCategoriesIds(List<String> subCategoriesIds) {
        this.subCategoriesIds = subCategoriesIds;
    }

    public List<Integer> getProductsIds() {
        return this.productsIds;
    }

    public String getName() {
        return this.name;
    }
}

class Product {

    private final int id;
    private List<UUID> salesIds = List.of();
    private final List<String> categoriesIds;

    public Product(int id, List<String> categoriesIds) {
        this.id = id;
        this.categoriesIds = categoriesIds;
    }

    public void setSales(List<Sale> sales) {
        this.salesIds = sales;
    }

    public List<UUID> getSalesIds() {
        return this.salesIds;
    }

    public int getId() {
        return this.id;
    }

    public List<String> getCategoriesIds() {
        return this.categoriesIds;
    }
}

class Sale {

    private final UUID id;
    private final int numberOfProductsSold;
    private final int productPrice;

    public Sale(int numberOfProductsSold, int productPrice) {
        this.id = UUID.randomUUID();
        this.numberOfProductsSold = numberOfProductsSold;
        this.productPrice = productPrice;
    }

    public int getProductPrice() {
        return this.productPrice;
    }

    public int getNumberOfProductsSold() {
        return this.numberOfProductsSold;
    }

    public UUID getId() {
        return id;
    }
}
