package org.example;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.*;

class Solution {
    private final RestTemplate restTemplate;

    public Solution(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private Stream<Category> getSubCategories(String categoryId) {
        Category rootCategory = restTemplate
                .getForEntity("/category/" + categoryId, Category.class)
                .getBody();
        if (rootCategory != null) {
            return Stream.concat(Stream.of(rootCategory),
                    rootCategory.getSubCategoriesIds().stream()
                            .flatMap(subCatId -> getSubCategories(subCatId)));//recursion
        } else return Stream.empty();
    }
//solution accepted by tests
    /*
    private Stream<Category> getSubCategories(String categoryId) {
        Category rootCategory = restTemplate
                .getForEntity("/category/" + categoryId, Category.class);

        if (rootCategory != null) {
            return Stream.concat(Stream.of(rootCategory),
                    rootCategory.getSubCategoriesIds().stream()
                            .flatMap(subCatId -> getSubCategories(subCatId)));
        } else return Stream.empty();
    }
    private List<Sale> getSalesForProdId(Integer prodId) {
        return restTemplate.getForEntity(
                "/product/" + prodId + "/sales",
                List.class);
    */

    private List<Sale> getSalesForProdId(Integer prodId) {
        ResponseEntity<Sale[]> response = restTemplate.getForEntity(
                "/product/" + prodId + "/sales",
                Sale[].class);
        if (response.getBody() != null) {
            return Arrays.stream(response.getBody()).toList();
        } else return List.of();
    }

    private Set<Integer> getUniqueProductIds(String category) {
        return getSubCategories(category)
                .flatMap(eachCategory -> eachCategory.getProductsIds().stream())
                .collect(Collectors.toSet());
    }

    public int getTotalIncome(String category) {
        return getUniqueProductIds(category).stream()
                .flatMap(prodId -> getSalesForProdId(prodId).stream())
                .map(sale -> sale.getProductPrice() * sale.getNumberOfProductsSold())
                .reduce(0, Integer::sum);
    }

    public int getNoOfProductsSold(String category) {
        return getUniqueProductIds(category).stream()
                .flatMap(prodId -> getSalesForProdId(prodId).stream())
                .map(Sale::getNumberOfProductsSold)
                .reduce(0, Integer::sum);
    }
}

class Category {

    private final String name;
    private List<Integer> productsIds = List.of();
    private List<String> subCategoriesIds = List.of();

    public Category(String name) {
        this.name = name;
    }

    public void setProductsIds(List<Integer> productsIds) {
        this.productsIds = productsIds;
    }

    public List<String> getSubCategoriesIds() {
        return subCategoriesIds;
    }

    public void setSubCategoriesIds(List<String> subCategoriesIds) {
        this.subCategoriesIds = subCategoriesIds;
    }

    public List<Integer> getProductsIds() {
        return this.productsIds;
    }

    public String getName() {
        return this.name;
    }
}

class Product {

    private final int id;
    private List<UUID> salesIds = List.of();
    private final List<String> categoriesIds;

    public Product(int id, List<String> categoriesIds) {
        this.id = id;
        this.categoriesIds = categoriesIds;
    }

    public void setSales(List<Sale> sales) {
        this.salesIds = sales.stream().map(Sale::getId).toList();
    }

    public List<UUID> getSalesIds() {
        return this.salesIds;
    }

    public int getId() {
        return this.id;
    }

    public List<String> getCategoriesIds() {
        return this.categoriesIds;
    }
}

class Sale {

    private final UUID id;
    private final int numberOfProductsSold;
    private final int productPrice;

    public Sale(int numberOfProductsSold, int productPrice) {
        this.id = UUID.randomUUID();
        this.numberOfProductsSold = numberOfProductsSold;
        this.productPrice = productPrice;
    }

    public int getProductPrice() {
        return this.productPrice;
    }

    public int getNumberOfProductsSold() {
        return this.numberOfProductsSold;
    }

    public UUID getId() {
        return id;
    }
}